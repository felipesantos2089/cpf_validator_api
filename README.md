# API para Validação de CPF

Projeto exemplo em NodeJS (https://nodejs.org) e Express (https://expressjs.com). Esse é um projeto apenas para uso didático.

A API recebe o número de cpf e retorna se é válido ou não.

Para rodar execute:

> node app.js

Acesse:

> http://localhost:3000

E para testar o retorno da API

> http://localhost:3000/validator?cpf=<um número de cpf>

Apesar do CPF contendo todos os números iguais pela regra de validação dos dígitos ser considerado válido, a API retorna falso. Por exemplo, o retorno da API para o CPF 000.000.000-00 seria:

```
{"cpf":"00000000000","result":false}
```
